module.exports = {
    httpPort: '3000',
    logger:  {
        //folder: '/var/log/bit2me', // TODO !!!!!!!!
        folder: '.',
        fileName: 'bit2me-%DATE%.log',
        level: 'debug',
        maxSize: '10m',
        maxFiles: '14d'
    },
    tasks : {
        'getPrices' : {
            //schedule: '*/5 * * * * *',
            schedule: '0 * * * * *',
            task: 'getPrices'
        },
        'sendEmail' : {
            //schedule: '*/5 * * * * *',
            schedule: '20 0 * * * *',
            task: 'sendEmail'
        }
    },
    mongoDbUrl: 'mongodb://mongo:27017',
    mongoDbName: 'bit2me',
    email : {
        user: 'bit2me.assignment@gmail.com',
        password: 'this_is_just_1_test',
        // TODO:
        // to: [
        //     'daviddq@gmail.com',
        //     'dev-test@team.bit2me.com'
        // ],
        to: [
            'daviddq@gmail.com'
        ],
        template: {
            en: {
                subject: 'Latest coin prices report',
                body: [
                    'Hello,',
                    '',
                    'Find below latest prices for each coin: ',
                    '',
                    '<table border=1><tr><th>Id</th><th>Name</th><th>Datetime</th><th>Price ($)</th></tr> %prices% </table>',
                    '',
                    'Follow realtime information at:',
                    '',
                    '<a href="http://daviddiaz.es">http://daviddiaz.es</a>',
                    '',
                    'Kind regards,',
                    '<b>Bit2Me assignment</b>'
                ],
                templateTableLine: '<tr><td>%id%</td><td>%name%</td><td>%time%</td><td align="right">%price%</td></tr>'
            },
            es: {
                subject: 'Informe de últimos precios',
                body: [
                    'Hola:',
                    '',
                    'A continuación encontrará las últimas cotizaciones para las diferentes monedas: ',
                    '',
                    '<table border=1><tr><th>Id</th><th>Nombre</th><th>Fecha</th><th>Precio ($)</th></tr> %prices% </table>',
                    '',
                    'Consulte la información en tiempo real en:',
                    '',
                    '<a href="http://daviddiaz.es">http://daviddiaz.es</a>',
                    '',
                    'Saludos,',
                    '<b>Bit2Me assignment</b>'
                ],
                templateTableLine: '<tr><td>%id%</td><td>%name%</td><td>%time%</td><td align="right">%price%</td></tr>'
            }
        },
    },
    coinsList: {
        1 :    { name: 'Bitcoin' },
        1027 : { name: 'Ethereum' },
        52 :   { name: 'XRP' },
        1831 : { name: 'Bitcoin Cash' },
        1765 : { name: 'EOS' },
        512 :  { name: 'Stellar' },
        2 :    { name: 'Litecoin' },
        1958 : { name: 'TRON' }
    }
}

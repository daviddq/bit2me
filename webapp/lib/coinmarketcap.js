﻿"use strict";

const request = require('request');

const URL_PRICES = 'https://api.coinmarketcap.com/v2/ticker/';

module.exports = (context) => {
  var module = {};

  module.getLatestPrices = () => {
    return new Promise( (resolve, reject) => {
      context.logger.verbose('[cmc] Querying "coinmarketcap" for prices...');

      request(URL_PRICES, (error, response, body) => {
        if (!error && response.statusCode == 200) {
          context.logger.verbose('[cmc] Querying "coinmarketcap" for prices... OK');
          resolve(body);
        }
        else {
          context.logger.error('[cmc] Querying "coinmarketcap" for prices... error:' + error);
          reject(error);
        }
      });
    });
  };

  return module;
};

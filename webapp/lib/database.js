﻿"use strict";

var MongoClient = require('mongodb').MongoClient;

const COLLECTION_HISTORY = 'pricesHistory';
const HISTORY_PERIOD = 100 * 60 * 1000; // 100 minutes in milliseconds

module.exports = (context) => {
  var module = {};

  // --------------------------------------------------------------------------
  // getDb
  //
  // Retrieves a reference to the database
  // --------------------------------------------------------------------------
  module.getDb = async () => {
    var client = await MongoClient.connect(context.config.mongoDbUrl,  { useNewUrlParser: true });
    var db = client.db(context.config.mongoDbName);

    return {client: client, db: db};
  };
  
  // --------------------------------------------------------------------------
  // storePrices
  // --------------------------------------------------------------------------
  module.storePrices = async (data) => {
    var currentTime = Date.now();

    let {client, db} = await module.getDb();
    
    context.logger.verbose('[db] Storing coin prices on database');
    
    try {
      Object.keys(data).forEach( async (key) => {
        var entry = data[key];
        
        if (entry.id in context.config.coinsList) {
          await db.collection(COLLECTION_HISTORY).updateOne(
            { coinId: entry.id },
            { 
              $push: { 
                history: { time: currentTime, price: entry.quotes.USD.price }
              }
            },
            { upsert: true }
          );
        }
      });
    }
    catch (ex) {
      context.logger.error('[db] module.storePrices', ex);
    }
    finally {
      client.close();
    }    
  };
    
  // --------------------------------------------------------------------------
  // getLatestCoinPrice
  // --------------------------------------------------------------------------
  module.getLatestCoinPrice = async (coinId) => {
    coinId = Number.parseInt(coinId);

    let {client, db} = await module.getDb();

    var price = undefined;

    try {
      context.logger.verbose('[db] Getting last coin price for coinId ' + coinId);

      let cursor = await db.collection(COLLECTION_HISTORY).aggregate([
        {$match: { coinId: coinId }},
        {$unwind:"$history"},
        {$project:{"history.time":1, "history.price":1}},
        {$sort:{"history.time":-1}},
        {$limit:1}
      ]);

      let rows = await cursor.toArray();
      if (rows && rows.length==1 && rows[0].history.price!==undefined) {
        price = rows[0].history;
      }
    }
    catch (ex) {
      context.logger.error('[db] module.getLatestCoinPrice', ex);
    }
    finally {
      client.close();
    }

    return price;
  }
  
  // --------------------------------------------------------------------------
  // getCoinHistory
  // --------------------------------------------------------------------------
  module.getCoinHistory = async (coinId) => {
    coinId = Number.parseInt(coinId);

    let {client, db} = await module.getDb();

    var history = {};

    try {
      context.logger.verbose('[db] Querying price for coinId ' + coinId);

      let cursor = await db.collection(COLLECTION_HISTORY).aggregate([
        {$match:   {coinId: coinId }},
          {
            $project: {
              history : {
                $filter: {
                  input: "$history",
                  as: "item",
                  cond: { $gt: [ "$$item.time", (Date.now() - HISTORY_PERIOD) ] }
                }        
              }
            }
          }
      ]);

      let rows = await cursor.toArray();
      if (rows && rows.length==1 && rows[0].history!==undefined) {
        history[coinId] = {};
        history[coinId].name = context.config.coinsList[coinId].name;
        history[coinId].history = rows[0].history;
      }
    }
    catch (ex) {
      context.logger.error('[db] module.getCoinHistory', ex);
    }
    finally {
      client.close();
    }

    return history;
  }

  // --------------------------------------------------------------------------
  // getAllHistories
  // --------------------------------------------------------------------------
  module.getAllHistories = async () => {
    var history = {};

    try {
      context.logger.verbose('[db] Getting all coin prices history from database');

      // TODO: (improvement) do all queries in parallel and wait with Promise.all()
      for (let coinId of Object.keys(context.config.coinsList)) {
        coinId = Number.parseInt(coinId);
        var singleHistory = await module.getCoinHistory(coinId);
        history[coinId] = singleHistory[coinId];
      };
    }
    catch (ex) {
      context.logger.error('[db] module.getAllHistories', ex);
    }

    return history;
  }

  // --------------------------------------------------------------------------
  // getAllHistories
  // --------------------------------------------------------------------------
  module.getAllPrices = async () => {
    var prices = {};
    
    try {
      context.logger.verbose('[db] Getting all coin prices from database');

      // TODO: (improvement) do all queries in parallel and wait with Promise.all()
      for (let coinId of Object.keys(context.config.coinsList)) {
        coinId = Number.parseInt(coinId);
        var latest = await module.getLatestCoinPrice(coinId);
        prices[coinId] = latest;
      };
    }
    catch (ex) {
      context.logger.error('[db] module.getAllPrices', ex);
    }

    return prices;
  }    
  
  return module;
};

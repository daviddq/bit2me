"use strict";

module.exports = (context) => {
    var winston = require('winston');
    require('winston-daily-rotate-file');

    var dailyRotateFileTransport = new (winston.transports.DailyRotateFile)({
        filename: context.config.logger.fileName,
        dirname: context.config.logger.folder,
        maxSize: context.config.logger.maxSize,
        maxFiles: context.config.logger.maxFiles,
        datePattern: 'YYYY-MM-DD',
        zippedArchive: true
    });

    var consoleTransport = new ( winston.transports.Console )({ 
        level: context.config.logger.level
    });
    
    winston.loggers.add('bit2meLogger', {
        format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.colorize(),
            winston.format.printf(info => {
                return `[${info.timestamp}] [${info.level}] ${info.message}`;
            })
        ),
        transports: [
            consoleTransport,
            dailyRotateFileTransport
        ]
    });

    return  winston.loggers.get('bit2meLogger');
}
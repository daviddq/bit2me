module.exports = (context) => {
  
  const createError = require('http-errors');
  const Express = require('express');
  const path = require('path');
  const cookieParser = require('cookie-parser');
  const expressWinston = require('express-winston');

  const indexRouter = require('./routes/index');
  const priceRouter = require('./routes/price')(context);
  
  const express = Express();

  express.set('views', path.join(__dirname, 'views'));
  express.set('view engine', 'pug');

  express.use(Express.json());
  express.use(Express.urlencoded({ extended: false }));
  express.use(cookieParser());
  express.use(Express.static(path.join(__dirname, 'public')));

  express.use(expressWinston.logger({
    winstonInstance: context.logger
  }));
  
  express.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });  
  
  express.use("/doc", Express.static(__dirname + '/public/swagger'));
  express.use('/price/:coinId', priceRouter.single);
  express.use('/history/:coinId', priceRouter.history);
  express.use('/history/', priceRouter.history);
  express.use('/stream', context.sse.init);
  express.use('/', indexRouter);

  // catch 404 and forward to error handler
  express.use(function(req, res, next) {
    next(createError(404));
  });

  // error handler
  express.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
  });
  
  return express;
}

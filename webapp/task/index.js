﻿const cron = require('node-cron');

module.exports = (context) => {
  const coinmarketcap = require('../lib/coinmarketcap')(context);

  module.init = () => {
    if (!context || !context.config || !context.config.tasks) {
      return;
    }

    const tasks = {
      'getPrices' : module.getPrices,
      'sendEmail' : module.sendEmail
    };

    for (let key of Object.keys(context.config.tasks)) {
      let entry = context.config.tasks[key];

      if (entry.task in tasks) {
        context.logger.info('[task] Scheduling task "' + entry.task + '" at ' + entry.schedule);
        cron.schedule(entry.schedule, tasks[entry.task] );
      }
      else {
        console.log('Task ', entry.task, ' not found.');
      }
    }
  };

  module.getPrices = async () => {
    context.logger.info('[task] Running task "getPrices"');

    const response = await coinmarketcap.getLatestPrices();
    await context.database.storePrices( JSON.parse(response).data );
    var history = await context.database.getAllHistories();
    if (history) {
      context.sse.send(history);
    }
  };

  module.sendEmail = async () => {
    context.logger.info('[task] Running task "sendEmail"');

    let prices = await context.database.getAllPrices();

    if (!prices || Object.keys(prices).length < 1) {
      return;
    }

    for (let language of ['en', 'es']) {
      let template = context.config.email.template[language];

      if (!template || Object.keys(template).length < 1) {
        continue;
      }
      
      let table = '';
      for (let coinId of Object.keys(prices)) {
        let entry = prices[coinId];
        let coinName = context.config.coinsList[coinId].name;
        let time = new Date();
        time.setTime(entry.time);
        
        let line = template.templateTableLine;
        
        line = line.replace('%id%', coinId);
        line = line.replace('%name%', coinName);
        line = line.replace('%time%', time.toLocaleString());
        line = line.replace('%price%', Number.parseFloat(entry.price).toFixed(2));
        
        table += line;
      }
      
      let htmlMessage = template.body.join('<br>\n').replace('%prices%', table);
  
      const send = require('gmail-send')({
        user: context.config.email.user,
        pass: context.config.email.password,
        to: context.config.email.to,
        subject: template.subject
      });
    
      let err = await send({ html: htmlMessage });
      context.logger.info('[task] Sent email to ' + context.config.email.to + ' [' + language + ']. ' + (err || ''));
    }
  };

  return module;
};

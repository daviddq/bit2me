var should = require('chai').should();

var context = {
  config: require('../config/config.js'),
  logger: {
    verbose: t => {},
    info: t => {},
    error: t => {}
  },
  database: {
    getLatestCoinPrice :  (coinId) => {
      return (coinId == 1) ? { time: 1, price: 1.0 } : undefined;
    },
    getCoinHistory : (coinId) => {
      if (coinId == 1) {
        return {
          "1" : {
            "name" : "Bitcoin",
            "history" : [
              { time: 1, price: 1.0 },
              { time: 2, price: 2.0 },
              { time: 3, price: 3.0 },
              { time: 4, price: 4.0 },
              { time: 5, price: 5.0 },
            ]
          }
        };
      }
      else {
        return {};
      }
    },
    getAllHistories : () => {
      return {
        "1" : {
          "name" : "Bitcoin",
          "history" : [
            { time: 1, price: 1.0 },
            { time: 2, price: 2.0 },
            { time: 3, price: 3.0 },
            { time: 4, price: 4.0 },
            { time: 5, price: 5.0 },
          ]
        },
        "1027" : {
          "name" : "Ethereum",
          "history" : [
            { time: 1, price: 1.0 },
            { time: 2, price: 2.0 },
            { time: 3, price: 3.0 },
            { time: 4, price: 4.0 },
            { time: 5, price: 5.0 },
          ]
        },
      };
    }
  }
};

var price = require('../routes/price.js')(context);

describe ("API", () => {
  describe ("Single price API", () => {
    it ("Query price for an existing coin", async () => {
      try {
        let thePrice = await price.single(
          { params : { coinId : 1 } },
          {
            status: (code) => {
              code.should.be.equal (200);
              return {
                send: (value) => {
                  value.time.should.be.equal(1);
                  value.price.should.be.equal(1.0);
                }
              };
            }
          }
        );
        
      }
      catch (ex) {
        throw ex;
      }
    });
      
    it ("Query price for non-existent coin", async () => {
      try {
        let thePrice = await price.single(
          { params : { coinId : 999 } },
          {
            status: (code) => {
              code.should.be.equal (404);
              return {
                send: (value) => {
                  value.err.should.be.equal (404);
                  value.errDescription.should.be.equal ('Not found');
                }
              };
            }
          }
        );
        
      }
      catch (ex) {
        throw ex;
      }
    });    
      
    it ("Query price without coinId", async () => {
      try {
        let thePrice = await price.single(
          { params : {  } },
          {
            status: (code) => {
              code.should.be.equal (400);
              return {
                send: (value) => {
                  value.err.should.be.equal (400);
                  value.errDescription.should.be.equal ('Bad parameters');
                }
              };
            }
          }
        );
        
      }
      catch (ex) {
        throw ex;
      }
    });
  });

    
  describe ("Price history API", () => {
    it ("Query history for a single existing coin", async () => {
      try {
        let theHistory = await price.history(
          { params : { coinId : 1 } },
          {
            status: (code) => {
              code.should.be.equal (200);
              return {
                send: (value) => {
                  value.err.should.be.equal(0);
                  value.errDescription.should.be.equal('');
                  should.exist(value.data);
                  should.exist(value.data["1"]);
                  should.exist(value.data["1"].name);
                  value.data["1"].name.should.be.equal('Bitcoin');
                  should.exist(value.data["1"].history);
                  value.data["1"].history.should.have.lengthOf(5);
                  value.data["1"].history[0].time.should.be.equal(1);
                  value.data["1"].history[0].price.should.be.equal(1.0);
                  value.data["1"].history[4].time.should.be.equal(5);
                  value.data["1"].history[4].price.should.be.equal(5.0);
                }
              };
            }
          }
        );
        
      }
      catch (ex) {
        throw ex;
      }
    });

    it ("Query history for a single non-existent coin", async () => {
      try {
        let theHistory = await price.history(
          { params : { coinId : 999 } },
          {
            status: (code) => {
              code.should.be.equal (404);
              return {
                send: (value) => {
                  value.err.should.be.equal(404);
                  value.errDescription.should.be.equal('Not found');
                  should.not.exist(value.data);
                }
              };
            }
          }
        );
        
      }
      catch (ex) {
        throw ex;
      }
    });

    it ("Query history for all coins", async () => {
      try {
        let theHistory = await price.history(
          { params : { } },
          {
            status: (code) => {
              code.should.be.equal (200);
              return {
                send: (value) => {
                  value.err.should.be.equal(0);
                  value.errDescription.should.be.equal('');
                  should.exist(value.data);
                  should.exist(value.data["1"]);
                  value.data["1"].name.should.be.equal('Bitcoin');
                  value.data["1"].history.should.have.lengthOf(5);
                  value.data["1"].history[0].time.should.be.equal(1);
                  value.data["1"].history[0].price.should.be.equal(1.0);
                  value.data["1"].history[4].time.should.be.equal(5);
                  value.data["1"].history[4].price.should.be.equal(5.0);
                  should.exist(value.data["1027"]);
                  value.data["1027"].name.should.be.equal('Ethereum');
                  value.data["1027"].history.should.have.lengthOf(5);
                  value.data["1027"].history[0].time.should.be.equal(1);
                  value.data["1027"].history[0].price.should.be.equal(1.0);
                  value.data["1027"].history[4].time.should.be.equal(5);
                  value.data["1027"].history[4].price.should.be.equal(5.0);
                }
              };
            }
          }
        );
        
      }
      catch (ex) {
        throw ex;
      }
    });
  });
});

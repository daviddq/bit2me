
module.exports = (context) => {
  var module = {};

  // --------------------------------------------------------------------------
  // single
  // --------------------------------------------------------------------------
  module.single = async (req, res, next) => {
    var response = { err: 0, errDescription: "" };

    var coinId = Number.parseInt(req.params.coinId);

    context.logger.verbose('[api] Querying price for coinId ' + coinId);

    if (!coinId || !Number.isInteger(coinId)) {
      context.logger.error('[api] Bad parameter coinId: ' + coinId);
      response.err = 400;
      response.errDescription = "Bad parameters";
      res.status(response.err).send(response);
      return;
    }

    var price = await context.database.getLatestCoinPrice(coinId);

    if (!price || !price.price || !price.time) {
      context.logger.error('[api] Information for coinId ' + coinId + ' was not found');
      response.err = 404;
      response.errDescription = "Not found";
      res.status(response.err).send(response);
      return;
    }
    
    response.coinId = coinId;
    response.name = context.config.coinsList[coinId].name;
    response.price = price.price;
    response.time = price.time;

    context.logger.verbose('[api] Price for coinId ' + coinId + ': ' + JSON.stringify(response));

    res.status(200).send(response);
  };

  // --------------------------------------------------------------------------
  // history
  // --------------------------------------------------------------------------
  module.history = async (req, res, next) => {
    var response = { err: 0, errDescription: "" };

    var coinId = Number.parseInt(req.params.coinId);

    context.logger.verbose('[api] Querying history for coinId ' + coinId);

    if (coinId && !Number.isInteger(coinId)) {
      context.logger.error('[api] Bad parameter coinId: ' + coinId);
      response.err = 400;
      response.errDescription = "Bad parameters";
      res.status(response.err).send(response);
      return;
    }

    var history = {};
    if (coinId) {
      history = await context.database.getCoinHistory(coinId);
    }
    else {
      history = await context.database.getAllHistories();
    }

    if (!history || Object.keys(history).length < 1) {
      context.logger.error('[api] Information for coinId ' + coinId + ' was not found');
      response.err = 404;
      response.errDescription = "Not found";
      res.status(response.err).send(response);
      return;
    }

    response.data = history;

    context.logger.verbose('[api] Retrieved history for ' + Object.keys(response.data).length + ' coins' );

    res.status(200).send(response);
  };

  return module;
}

const TEMPLATE_DIV_NAME = 'template_div';

var globalData = [];
var charts = {};

fetch('/history', {mode: 'cors'})
  .then(function(response) {
    response.json().then(function(bodyContent) {
      globalData = bodyContent.data;

      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawCharts);
      
      // Listen to server sent events
      var evtSource = new EventSource("/stream");
      evtSource.onmessage = function (e) {
        drawCharts(JSON.parse(e.data));
      };
    });
  })
  .catch(function(error) {
    console.log(error);
  });

function json2array(jsonData) {
  var arrayData = [
    ["Time", "Price"]
  ];

  for (idx in jsonData) {
    var entry = jsonData[idx];
    var date = new Date();
    date.setTime(entry.time)
    entry.time = date;

    arrayData.push([entry.time, entry.price]);
  }
  
  return arrayData;
}

function drawCharts(data) {
  // First time data is rendered data is provided in "globalData". After that,
  // "server sent events" provide data in "data" parameter.
  data = data || globalData;
   
  for (key in data) {
    var chartId = "chart_" + key;
    var coinName = data[key].name;
    var history = data[key].history;
    var arrayData = json2array(history);

    createDivIfNotExist(chartId);
    drawChart(coinName, arrayData, chartId);
  }
}

var left = true;
function createDivIfNotExist(newId) {
  var div = document.getElementById(newId);
  if (div != null) {
    return;
  }

  div = document.getElementById(TEMPLATE_DIV_NAME);
  var clone = div.cloneNode(true);
  clone.id = newId;

  var column = left ? document.getElementById('leftColumn') : document.getElementById('rightColumn');
  (column || document.body).appendChild(clone);

  left = !left;
}

function drawChart(coinName, sourceData, chartId) {
  var data = google.visualization.arrayToDataTable(
    sourceData
  );

  var options = {
    legend: 'none',
    width:  500,
    height: 150,
    title:  coinName,
    vAxis: {
      gridlines: { color: 'transparent' }
    },
    hAxis: {
      gridlines: { color: 'transparent' }
    }
  };

  if ( !charts[chartId] ) {
    charts[chartId] = new google.visualization.AreaChart( document.getElementById(chartId) );
  }
  charts[chartId].draw(data, options);
}

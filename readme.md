Bit2Me assignment
=================

Git repository: https://gitlab.com/daviddq/bit2me

Deployed app: http://daviddiaz.es

Documentation: https://goo.gl/DA82nC

Quick start
-----------

### Steps to install the app:

- Make sure you have a system with latest node.js and npm installed and properly working

- Clone the application:

   `sudo git clone https://gitlab.com/daviddq/bit2me.git` 

- Install dependencies

  `cd ./bit2me/webapp`

  `npm install`

### Steps to run the tests

- To launch the tests, run the follofing commands:

  `cd ./bit2me/webapp`

  `npm test`

### Steps to run the raw app (without docker):

- Make sure the app is correctly installed

- Make sure you have a valid and running instance of MongoDB

- Setup MongoDB params on ./bit2me/webapp/config/config.js file

- Run the following commands:

  `cd ./bit2me/webapp`

  `npm start`

- You should be able to access the site on:

  http://[host machine ip]:3000

- Find the API documentation on:

  http://[host machine ip]:3000/doc

### Steps to run the dockerized app:

- Clone the application:

   `sudo git clone https://gitlab.com/daviddq/bit2me.git` 

- Run the docker composition:

  `cd ./bit2me/webapp`

  `docker-compose up`


Deployment
----------

- Create an AWS EC2 instance

- Log in into the EC2 instance

- Install and launch Docker:

  `sudo yum -y install docker`

  `sudo service docker start`

- Install docker-compose

  `sudo yum -y install python-pip`

  `sudo pip install docker-compose`

- Add user to "docker" group

  `sudo usermod -aG docker $USER`

- Install git

  `sudo yum -y install git-core`

- Make any changes you need to the config file (/usr/src/bit2me/webapp/config/config.js)

- Clone the app

   `cd /usr/src/`
   `sudo git clone https://gitlab.com/daviddq/bit2me.git` 

- Launch the infrastructure:

    `cd /usr/src/bit2me/webapp`
    
    `docker-compose up`
